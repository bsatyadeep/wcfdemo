﻿using System;
using System.ServiceModel;
using System.Windows.Forms;
using WCFDemoTest.ServiceReference1;
using WCFDemoTest.ServiceReference2;

namespace WCFDemoTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void btnWCFDemoDay1Invoke_Click(object sender, EventArgs e)
        {
            MyServiceClient client = new MyServiceClient();
            this.txtResult.Text = String.Empty;
            this.txtResult.Text += "Client calling the service...\r\n";
            this.txtResult.Text += "Counter: " + client.MyMethod() + "\r\n";
            this.txtResult.Text += "Counter: " + client.MyMethod() + "\r\n";
            this.txtResult.Text += "Counter: " + client.MyMethod() + "\r\n";
            this.txtResult.Text += "Counter: " + client.MyMethod() + "\r\n";
            this.txtResult.Text += "Counter: " + client.MyMethod() + "\r\n";
            this.txtResult.Text += "Counter: " + client.MyMethod() + "\r\n";

            MyServiceClient client2 = new MyServiceClient();
            //this.txtResult.Text = String.Empty;
            this.txtResult.Text += "Client calling the service...\r\n";
            this.txtResult.Text += "Counter: " + client2.MyMethod() + "\r\n";
            this.txtResult.Text += "Counter: " + client2.MyMethod() + "\r\n";
            this.txtResult.Text += "Counter: " + client2.MyMethod() + "\r\n";
            this.txtResult.Text += "Counter: " + client2.MyMethod() + "\r\n";
            this.txtResult.Text += "Counter: " + client2.MyMethod() + "\r\n";
            this.txtResult.Text += "Counter: " + client2.MyMethod() + "\r\n";


            MyCalculatorServiceClient client1 = new MyCalculatorServiceClient();

            this.txtResult.Text += "Client Calling the service...\r\n";
            this.txtResult.Text += this.txtNum1.Text + "+" + this.txtNum2.Text +" = " +  client1.Add(int.Parse(this.txtNum1.Text), int.Parse(this.txtNum2.Text));
            this.txtResult.Text += "\r\n";
            this.txtResult.Text += this.txtNum1.Text + "-" + this.txtNum2.Text + " = " + client1.Subtract(int.Parse(this.txtNum1.Text), int.Parse(this.txtNum2.Text));
            this.txtResult.Text += "\r\n";

            try
            {
                this.txtResult.Text += this.txtNum1.Text + "\\" + this.txtNum2.Text + " = " + client1.Divide(int.Parse(this.txtNum1.Text), int.Parse(this.txtNum2.Text));
                this.txtResult.Text += "\r\n";
            }
            catch (FaultException<CustomException> ex)
            {
                this.txtResult.Text += ex.Detail.ExceptionMessage;
            }
        }
    }
}
