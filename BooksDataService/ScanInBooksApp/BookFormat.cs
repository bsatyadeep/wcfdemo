﻿namespace ScanInBooksApp.BooksServiceReference
{
    public enum BookFormat
    {
        Unknown,
        Hardcover,
        TradePaperback,
        MassMarketPaperback,
        Electronic,
    }
}
