﻿using System.Collections.ObjectModel;
using System.Data.Services.Client;
using System.Linq;
using ScanInBooksApp.BooksServiceReference;

namespace ScanInBooksApp
{
    public class MainWindowViewModel : ViewModelBase
    {
        private ObservableCollection<Title> _books = new ObservableCollection<Title>();

        public ObservableCollection<Title> Books
        {
            get { return _books; }
            set { _books = value; }
        }
        private Title _SelectedBook;
        private QueryOperationResponse<Title> _queryoperationresponse;
        private BooksEntities _context;

        public Title SelectedBook
        {
            get { return _SelectedBook; }
            set
            {
                if (value != null || value != _SelectedBook)
                {                    
                    _SelectedBook = value;              
      

                    //for explicit loading
                    //var context = ContextFactory.CreateContext();
                    //context.AttachTo("Title",SelectedBook);
                    //context.LoadProperty(SelectedBook, "Authors");
                    //var primaryAuthor = SelectedBook.Authors.First();
                    //SelectedBook.PrimaryAuthor = primaryAuthor.FirstName + " " + primaryAuthor.LastName;
                    //RaisePropertyChanged("SelectedBook");

                }
            }
        }
        public void QueryForBooks()
        {
            _books.Clear();
			_context = ContextFactory.CreateContext();
            {
                //var dataServiceQuery = (DataServiceQuery<Title>) _context.Titles.Expand("Authors");
                //var query = dataServiceQuery.OrderBy(one=>one.BookTitle);
                //IEnumerable<Title> reponse = dataServiceQuery.Execute();
                //_queryoperationresponse = (QueryOperationResponse<Title>) reponse;
                //foreach (var title in reponse)
                //{
                //    _books.Add(title);
                //}
                IOrderedQueryable<Title> query = (IOrderedQueryable<Title>) _context.Titles.Expand("Authors").OrderBy(one => one.BookTitle);
                var reponse = ((DataServiceQuery)query).Execute();
                _queryoperationresponse = (QueryOperationResponse<Title>)reponse;
                foreach (Title title in reponse)
                {
                    _books.Add(title);
                }
            }
        }

        public void Next()
        {
            var continuation = _queryoperationresponse.GetContinuation();
            if (continuation != null)
            {
                _books.Clear();
                var query = _context.Execute<Title>(continuation);
                _queryoperationresponse = (QueryOperationResponse<Title>)query;
                foreach (var title in query)
                {
                    _books.Add(title);
                }
            }
        }
    }
}