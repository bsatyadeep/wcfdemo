﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;
using ScanInBooksApp.BooksServiceReference;

namespace ScanInBooksApp
{
    public class IsbnDbBookLookup : IBookLookup
    {
        public async Task<string> Lookup(string isbn)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri("http://www.isbndb.com/api/books.xml"),
            };
            string query = string.Format(@"?access_key={0}&results=details&index1=isbn&value1={1}", "ESVDVCRH", isbn);

            Uri uriPath = new Uri(query, UriKind.Relative);
            //string uriPath = string.Format("?access_key={0}&results=details&index1=isbn&value1={1}", "ESVDVCRH", isbn);
            var result = await client.GetAsync(uriPath);
            return await result.Content.ReadAsStringAsync();
        }

        public Title ParseTitle(string textResult)
        {
            var xml = XElement.Parse(textResult);
            //<ISBNdb server_time="2005-07-29T03:02:22">
            // <BookList total_results="1">
            //  <BookData book_id="paul_laurence_dunbar" isbn="0766013502">
            //   <Title>Paul Laurence Dunbar</Title>
            //   <TitleLong>Paul Laurence Dunbar: portrait of a poet</TitleLong>
            //   <AuthorsText>Catherine Reef</AuthorsText>
            //   <PublisherText publisher_id="enslow_publishers">
            //    Berkeley Heights, NJ: Enslow Publishers, c2000.
            //   </PublisherText>
            //   <Summary>
            //    A biography of the poet who faced racism and devoted himself
            //    to depicting the black experience in America.
            //   </Summary>
            XElement bookData = xml.Element("BookList").Element("BookData");
            var title = new Title()
            {
                BookTitle = bookData.Element("Title").Value,
            };
            return title;
        }
        public string TextResult { get; set; }
    }
}
