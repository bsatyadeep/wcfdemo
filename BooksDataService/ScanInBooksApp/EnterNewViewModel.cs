﻿using System;
using System.Data.Services.Client;
using System.Linq;
using ScanInBooksApp.BooksServiceReference;

namespace ScanInBooksApp
{
    public class EnterNewViewModel : ViewModelBase
    {
        private string _textResult;
        private string _isbn;
        private Title _title;
        private bool _isNew;

        public async void LookupBook(string isbn)
        {
            //var lookup = new AmazonBookLookup();
            var lookup = new IsbnDbBookLookup();
            var task = lookup.Lookup(isbn);
	        var context = new BooksEntities(Constants.ServiceUri);
            {
                context.IgnoreResourceNotFoundException = true;
                Title = context.Titles.Where(t => t.ISBN == isbn).FirstOrDefault();
                if (Title != null)
                {
                    IsNew = false;
                    return;
                }
            }
            TextResult = await task;
            _title = new Title();
            Title = lookup.ParseTitle(TextResult);
            IsNew = true;
        }

        public string Isbn
        {
            get { return _isbn; }
            set
            { 
                _isbn = value;
                RaisePropertyChanged();
                if (!string.IsNullOrEmpty(_isbn))
                {
                    LookupBook(_isbn);
                }
            }
        }

        public string TextResult
        {
            get { return _textResult; }
            set { _textResult = value; RaisePropertyChanged(); }
        }

        public Title Title
        {
            get { return _title; }
            set { _title = value; RaisePropertyChanged(); }
        }

        public bool IsNew
        {
            get { return _isNew; }
            set { _isNew = value; RaisePropertyChanged(); }
        }

        public void Save()
        {
			var context = new BooksEntities(Constants.ServiceUri);
			{
                if (Title.ISBN == null)
                {
                    Title.ISBN = Isbn;
                }
				context.AddToTitles(Title);
				LookupAuthorByText(context, Title);
                LookupPublisherByText(context, Title);
                context.SaveChanges(SaveChangesOptions.Batch);
                ClearProperties();
            }
        }

        private void ClearProperties()
        {
            Isbn = null;
            Title = null;
            TextResult = null;
        }

        private void LookupPublisherByText(BooksEntities context, Title title)
        {
            if (title.PublisherText != null)
            {
                context.IgnoreResourceNotFoundException = true;
				var publisher = context.Publishers.Where(p => p.Name == title.PublisherText).FirstOrDefault();
                if (publisher == null)
                {
                    publisher = new Publisher {Name = title.PublisherText};
					context.AddToPublishers(publisher);
                }
                title.Publisher = publisher;
				context.SetLink(title, "Publisher", publisher);
				context.AddLink(publisher, "Titles", title);
            }
        }

        private void LookupAuthorByText(BooksEntities context, Title title)
        {
            if (title.AuthorsText != null)
            {
                string[] authors = title.AuthorsText.Split(',');
                foreach (string authorText in authors)
                {
                    string[] nameParts = authorText.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                    string firstName = nameParts.First();
                    string lastName = nameParts.Last();
                    var author =
						context.Authors.Where(a => a.FirstName == firstName && a.LastName == lastName).FirstOrDefault();
                    if (author == null)
                    {
                        author = new Author {FirstName = firstName, LastName = lastName,};
                        context.AddToAuthors(author);
                    }
                    title.Authors.Add(author);
					author.Titles.Add(title);
					context.AddLink(author, "Titles", title);
					context.AddLink(title, "Authors", author);
				}
            }
        }
    }
}
