﻿using System.Windows;

namespace ScanInBooksApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            buttonAddNew.Click += AddNew;
            btnRefresh.Click += btnRefresh_Click;
            btnNext.Click += btnNext_Click;
            ViewModel = new MainWindowViewModel();
            ViewModel.QueryForBooks();
        }

        void btnNext_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.Next();
        }

        void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.QueryForBooks();
        }

        public MainWindowViewModel ViewModel
        {
            get { return (MainWindowViewModel)DataContext; }
            set { DataContext = value; }
        }

        public void AddNew(object sender, RoutedEventArgs routedEventArgs)
        {
            var window = new EnterNewWindow();
            window.Show();
        }
    }
}
