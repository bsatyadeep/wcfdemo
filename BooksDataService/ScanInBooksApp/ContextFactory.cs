﻿using System.Data.Services.Client;
using ScanInBooksApp.BooksServiceReference;

namespace ScanInBooksApp
{
    public static class ContextFactory
    {
        public static BooksEntities CreateContext()
        {
            var context = new BooksEntities(Constants.ServiceUri);
            context.MergeOption = MergeOption.PreserveChanges;
            return context;
        }
    }
}
