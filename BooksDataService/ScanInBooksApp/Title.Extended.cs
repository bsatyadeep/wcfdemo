﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
namespace ScanInBooksApp.BooksServiceReference
{
	public partial class Title 
    {
        //internal Author PrimaryAuthor { get { return Authors.FirstOrDefault(); } }

        public string PrimaryAuthor
        {
            get { return Authors.First().FirstName + " " + Authors.First().LastName; }
        }

	    //For explicit Loading
        //private string _PrimaryAuthor = String.Empty;
        //public string PrimaryAuthor
        //{
        //    get { return _PrimaryAuthor; }
        //    set
        //    {
        //        if (value != null || value != _PrimaryAuthor)
        //        {
        //            _PrimaryAuthor = value;
        //            OnPropertyChanged("PrimaryAuthor");
        //        }
        //    }
        //}

        [NotMapped]
		internal string AuthorsText { get; set; }
        [NotMapped]
		internal string PublisherText { get; set; }

        [NotMapped]
		internal string CommaSeparatedTags
        {
            get
            {
                string pipes = PipeSeparatedTags;
                if (string.IsNullOrEmpty(pipes)) return pipes;
                string[] tokens = pipes.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                return string.Join(", ", tokens);
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    PipeSeparatedTags = value;
                    return;
                }
                var tokens = value.Split(',').Select(s => s.Trim());
                PipeSeparatedTags = "|" + string.Join("|", tokens) + "|";
            }
        }

        [NotMapped]
		internal BookFormat BookFormat
        {
            get { return (BookFormat) Format; }
            set { Format = (int) value; }
        }

    }
}
