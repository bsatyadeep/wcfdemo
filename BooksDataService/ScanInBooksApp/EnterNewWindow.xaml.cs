﻿using System.Windows;

namespace ScanInBooksApp
{
    /// <summary>
    /// Interaction logic for EnterNewWindow.xaml
    /// </summary>
    public partial class EnterNewWindow : Window
    {
        public EnterNewWindow()
        {
            InitializeComponent();
            ViewModel = new EnterNewViewModel();
            textBoxIsbn.Focus();
            buttonAccept.Click += AcceptClicked;
            buttonCancel.Click += CancelClicked;
        }

        private void CancelClicked(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void AcceptClicked(object sender, RoutedEventArgs e)
        {
            ViewModel.Title.CommaSeparatedTags = textBoxTags.Text;
            ViewModel.Title.IsFiction = checkBoxFiction.IsChecked == true;
            ViewModel.Save();
            textBoxIsbn.Focus();
        }

        public EnterNewViewModel ViewModel
        {
            get { return (EnterNewViewModel) DataContext; } 
            set { DataContext = value; }
        }
    }
}
