﻿using System.Linq;
using System.Threading.Tasks;
using ScanInBooksApp.BooksServiceReference;

namespace ScanInBooksApp
{
    public interface IBookLookup
    {
        Task<string> Lookup(string isbn);
        Title ParseTitle(string textResult);
    }
}
