﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;
using AmazonProductAdvtApi;

using ScanInBooksApp.BooksServiceReference;

namespace ScanInBooksApp
{
    class AmazonBookLookup : IBookLookup
    {
        private const string AWS_ACCESS_KEY_ID = "AKIAIIJ5CN4QN43EJUVQ";
        private const string AWS_SECRET_KEY = "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";
        private const string DESTINATION = "webservices.amazon.com";

        public async Task<string> Lookup(string isbn)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri("http://webservices.amazon.com/onca/xml"),
            };

            var helper = new SignedRequestHelper(AWS_ACCESS_KEY_ID, AWS_SECRET_KEY, DESTINATION);
            var requestDictionary = new Dictionary<string, string>()
                {
                    {"Service", "AWSECommerceService"},
                    {"Operation", "ItemLookup"},
                    {"ResponseGroup", "Large"},
                    {"SearchIndex", "All"},
                    {"IdType", "ISBN"},
                    {"ItemId", isbn},
                    {"AWSAccessKeyId", AWS_ACCESS_KEY_ID},
                    {"AssociateTag", "persolibra07-20"},
                    {"Timestamp", DateTime.UtcNow.ToString("yyyy-MM-ddThh:mm:ssZ")},
                };

            var url = helper.Sign(requestDictionary);
            var result = await client.GetAsync(url);
            var xml = await result.Content.ReadAsStringAsync();
            var xelem = XElement.Parse(xml);
            return xelem.ToString();
        }

        public Title ParseTitle(string textResult)
        {
            XNamespace ns = "http://webservices.amazon.com/AWSECommerceService/2011-08-01";
            var xelem = XElement.Parse(textResult);

            XElement items = xelem.Element(ns + "Items");
            if (items == null) return null;
            XElement item = items.Element(ns + "Item");
            if (item == null) return null;
            var itemAttributes = item.Element(ns + "ItemAttributes");
            if (itemAttributes == null) return null;

            var title = new Title();
            XElement xIsbn = itemAttributes.Element(ns + "ISBN");
            if (xIsbn != null)
            {
                title.ISBN = xIsbn.Value;
            }
            XElement xAuthor = itemAttributes.Element(ns + "Author");
            if (xAuthor != null)
            {
                title.AuthorsText = xAuthor.Value;
            }
            var xElement = itemAttributes.Element(ns + "Title");
            if (xElement != null)
                title.BookTitle = xElement.Value;
            int indexOfColon = title.BookTitle.IndexOf(':');
            if (indexOfColon > 0)
            {
                title.FullTitle = title.BookTitle;
                title.BookTitle = title.FullTitle.Substring(0, indexOfColon);
            }
            XElement xPubDate = itemAttributes.Element(ns + "PublicationDate");
            if (xPubDate != null)
            {
                title.PublishDate = ParsePublishDate(xPubDate.Value);
            }
            XElement xPublisher = itemAttributes.Element(ns + "Publisher");
            if (xPublisher != null)
            {
                title.PublisherText = xPublisher.Value;
            }
            XElement numPages = itemAttributes.Element(ns + "NumberOfPages");
            if (numPages != null)
            {
                title.NumberOfPages = Convert.ToInt32(numPages.Value);
            }
            var element = itemAttributes.Element(ns + "Binding");
            if (element != null)
                title.BookFormat = ParseFormat(element.Value);
            return title;
        }

        private BookFormat ParseFormat(string value)
        {
            if (value == "Hardcover")
                return BookFormat.Hardcover;
            if (value == "Paperback")
                return BookFormat.TradePaperback;
            if (value == "Mass Market Paperback")
                return BookFormat.MassMarketPaperback;
            return BookFormat.Unknown;
        }

        private DateTime ParsePublishDate(string value)
        {
            if (value.Length == 4)
            {
                return DateTime.ParseExact(value, "yyyy", CultureInfo.InvariantCulture);
            }
            return DateTime.Parse(value);
        }
    }
}
