﻿using System;
using System.Linq;
using Books.Client.BooksService;

namespace Books.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var ctx = new BooksEntities(new Uri("http://localhost:61537/BooksDataService.svc"));
            var query = new Uri("Titles",uriKind:UriKind.Relative);
            var titles = ctx.Execute<Title>(query);
            var titleList = titles.ToList();

            Console.WriteLine("Total number of Titles: {0}" , titleList.Count);
            foreach (var title in titleList)
            {
                Console.WriteLine("Book Title: {0}", title.BookTitle);
            }
            var authers = (from one in ctx.Authors
                           where one.FirstName == "Abraham"
                select one);

            foreach (var auther in authers.ToList())
            {
                Console.WriteLine(auther.FirstName + " " + auther.LastName);
            }
            Author newauther = new Author {FirstName = "Satyadeep", LastName = "Behera"};
            ctx.AddToAuthors(newauther);
            ctx.SaveChanges();

            Console.ReadKey();
        }
    }
}
