//------------------------------------------------------------------------------
// <copyright file="WebDataService.svc.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Data.Services;
using System.Data.Services.Common;
using System.Globalization;
using System.ServiceModel;
using System.ServiceModel.Web;
using Books.Data;

namespace Books.Services
{    
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class BooksDataService : DataService<BooksEntities>
    {
        // This method is called only once to initialize service-wide policies.
        public static void InitializeService(DataServiceConfiguration config)
        {
            // TODO: set rules to indicate which entity sets and service operations are visible, updatable, etc.
            // Examples:
            config.SetEntitySetAccessRule("*", EntitySetRights.All);
            config.SetEntitySetPageSize("Titles",10);
            config.SetServiceOperationAccessRule("*", ServiceOperationRights.AllRead);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;
        }

        [WebGet]
        public string GetCurrentDateTime()
        {
            return DateTime.Now.ToString(CultureInfo.InvariantCulture);
        }
        //[QueryInterceptor("Titles")]
        //public Expression<Func<Title, bool>> OnQueryBooksTitle()
        //{
        //    return t => t.PipeSeparatedTags.Contains("business");
        //}
    }
}