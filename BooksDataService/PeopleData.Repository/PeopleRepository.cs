﻿using System.Collections.Generic;
using System.Linq;

namespace PeopleData.Repository
{
    public class PeopleRepository
    {
        private readonly List<Person> _People = new List<Person>
        {
            new Person() {Name = "Pramod", Age = 21},
            new Person() {Name = "Srinidhi", Age = 20},
            new Person() {Name = "Rakesh", Age = 20},
            new Person() {Name = "Kiran", Age = 23}
        };
        public IQueryable<Person> People
        {
            get { return _People.AsQueryable(); }            
        }
    }
}
