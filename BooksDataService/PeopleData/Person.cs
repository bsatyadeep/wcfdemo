﻿using System.Data.Services.Common;

namespace PeopleData
{
    [DataServiceKey("Name")]
    [EntityPropertyMapping("Name",
        SyndicationItemProperty.Title,
        SyndicationTextContentKind.Plaintext, 
        keepInContent:true)]
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}