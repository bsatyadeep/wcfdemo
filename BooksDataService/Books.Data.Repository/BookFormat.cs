﻿namespace Books.Data.Repository
{
    public enum BookFormat
    {
        Unknown,
        Hardcover,
        TradePaperback,
        MassMarketPaperback,
        Electronic
    }
}
