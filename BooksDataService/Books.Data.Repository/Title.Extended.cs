﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.Data.Repository
{
    public partial class Title
    {
        internal Author PrimeryAuther{get { return Authors.FirstOrDefault(); }}
        [NotMapped]
        internal string AuthorsText { get; set; }
        [NotMapped]
        internal string PublisherText { get; set; }

        [NotMapped]
        internal string CommaSeparatedTags
        {
            get
            {
                string pipes = PipeSeparatedTags;
                if (string.IsNullOrEmpty(pipes)) return pipes;
                string[] tokens = pipes.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                return string.Join(", ", tokens);
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    PipeSeparatedTags = value;
                    return;
                }
                var tokens = value.Split(',').Select(s => s.Trim());
                PipeSeparatedTags = "|" + string.Join("|", tokens) + "|";
            }
        }

        [NotMapped]
        internal BookFormat BookFormat
        {
            get { return (BookFormat)Format; }
            set { Format = (int)value; }
        }

    }
}