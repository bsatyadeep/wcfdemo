﻿using System;
using RoutingService.Common;
using RoutingServiceClient.StoredConnectionStringService;

namespace RoutingServiceClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new ConnectionStringServiceClient();
            ConnectionString builder = client.GetConnectionString("sa", "system", "DefaultConnectionString");

            //Debug.Assert(builder != null,string.Format("Connection String builder was{0} returned null, because the credntial were not valid or connecting string does not exist."));

            Console.WriteLine("Data Source=" +builder.DataSource +";Initial Catalog="+ builder.InitialCatalog+";User Id="+ builder.UserId+";Password="+builder.Password+";");
            Console.WriteLine(builder.ToString());
            Console.ReadLine();
        }
    }
}