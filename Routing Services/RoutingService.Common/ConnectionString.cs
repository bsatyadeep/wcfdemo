﻿using System.Data.SqlClient;
using System.Runtime.Serialization;

namespace RoutingService.Common
{
    [DataContract]
    public class ConnectionString
    {
        public ConnectionString(SqlConnectionStringBuilder connectionString)
        {
            DataSource = connectionString.DataSource;
            InitialCatalog = connectionString.InitialCatalog;
            UserId = connectionString.UserID;
            Password = connectionString.Password;
        }
        public ConnectionString(string datasource, string initialcatalog, string userid, string password)
        {
            DataSource = datasource;
            InitialCatalog = initialcatalog;
            UserId = userid;
            Password = password;
        }

        [DataMember]
        public string DataSource { get; set; }

        [DataMember]
        public string InitialCatalog { get; set; }

        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public string Password { get; set; }

        public override string ToString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder()
            {
                DataSource = DataSource??string.Empty,
                InitialCatalog = InitialCatalog??string.Empty,
                UserID = UserId??string.Empty,
                Password = Password??string.Empty
            };
            return builder.ToString();
        }
    }
}