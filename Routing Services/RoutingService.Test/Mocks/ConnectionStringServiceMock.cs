﻿using System.Collections.Generic;
using System.Data.SqlClient;
using RoutingService.Common;

namespace RoutingService.Test.Mocks
{
    class ConnectionStringServiceMock: ConnectionStringService
    {
        private const string UserName = "TestUser";
        private const string Password = "TestPassword";
        public ConnectionStringServiceMock()
        {
            _ConnectionString = new Dictionary<string, ConnectionString>
            {
                {
                    "DefaultConnectionString",
                    new ConnectionString(new SqlConnectionStringBuilder("Data Source=myServerAddress;Initial Catalog=myDataBase;User Id=myUserName;Password=myPassword;"))
                }
            };
        }
        public override ConnectionString GetConnectionString(string userName, string password, string name)
        {
            if (!UserName.Equals(userName) || !Password.Equals(password))
            {
                return null;
            }
            if (_ConnectionString.ContainsKey(name))
            {
                return _ConnectionString[name];
            }
            else
            {
                return null;
            }
        }
    }
}
