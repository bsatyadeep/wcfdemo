﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RoutingService.Common;
using RoutingService.Test.Mocks;

namespace RoutingService.Test
{
    [TestClass]
    public class ConnectionStringServiceTest
    {
        [TestMethod]
        public void GetConnectionStringTest()
        {
            ConnectionStringServiceMock service = new ConnectionStringServiceMock();
            ConnectionString builder = service.GetConnectionString("TestUser", "TestPassword", "DefaultConnectionString");
            Assert.IsNotNull(builder.ToString());
        }
    }
}