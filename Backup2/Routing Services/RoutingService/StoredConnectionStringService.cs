﻿using System.Collections.Generic;
using System.Data.SqlClient;
using RoutingService.Common;

namespace RoutingService
{
    /// <summary>
    /// Serves named ConnectionString
    /// </summary>
    public class StoredConnectionStringService:ConnectionStringService
    {
        #region Constant
        private const string UserName = "sa";
        private const string Password = "system";
        #endregion

        /// <summary>
        /// Initialize a new Instance of RoutingService.StoredConnectionString class.
        /// </summary>
        public StoredConnectionStringService()
        {
            _ConnectionString = new Dictionary<string, ConnectionString>()
            {
                {
                    "DefaultConnectionString",
                    new ConnectionString(new SqlConnectionStringBuilder(
                        "Data Source=myServerAddress;Initial Catalog=myDataBase;User Id=myUserName;Password=myPassword;"))
                }
            };
        }

        #region Public Interface
        /// <summary>
        /// Gets the Specified connectionstring using specified credentials by key name
        /// </summary>       
        public override ConnectionString GetConnectionString(string userName, string password, string name)
        {
            if (!UserName.Equals(userName) || !Password.Equals(password))
            {
                return null;
            }           
            else
            {
                return GetInternalConnectionString(name);
            }
        }
        #endregion

        #region Implementation
        private ConnectionString GetInternalConnectionString(string name)
        {
            if (_ConnectionString.ContainsKey(name))
            {
                return _ConnectionString[name];
            }
            else
            {
                return null;
            }
        }
        #endregion
    }
}
