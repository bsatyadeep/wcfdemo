﻿using System;
using System.ServiceModel;
using RoutingService.Common;

namespace RoutingService
{
    [ServiceContract]
    public interface IConnectionStringService
    {        
        [OperationContract]
        ConnectionString GetConnectionString(String userName, String password, String name);
    }
}