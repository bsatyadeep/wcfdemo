﻿using System;
using System.Collections.Generic;
using RoutingService.Common;

namespace RoutingService
{
    public abstract class ConnectionStringService : IConnectionStringService
    {
        protected Dictionary<String,ConnectionString> _ConnectionString;

        #region IConnectionStringService Members
        public abstract ConnectionString GetConnectionString(string userName,
            string password, string name);
        #endregion
    }
}