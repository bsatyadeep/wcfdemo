﻿using System;

namespace WCFDemoDay1
{
    //[ServiceBehavior(InstanceContextMode=InstanceContextMode.PerSession)]
    [Serializable]
    //[DurableService()]
    public class MyService : IMyService
    {       
        static int m_Counter = 0;  
        //[DurableOperation(CanCreateInstance=true)]
        public int MyMethod()
        {
            //OperationContext.Current.InstanceContext.ReleaseServiceInstance();
            m_Counter++;
            return m_Counter;
        } 
    }
}
