﻿using System;
using System.ServiceModel;

namespace WCFDemoDay1
{    
    public class MyCalculatorService : IMyCalculatorService
    {
        public int Add(int Num1, int Num2)
        {
            return Num1 + Num2;
        }        
        public int Subtract(int Num1, int Num2)
        {
            return Num1 - Num2;
        }
        public int Multiply(int num1, int num2)
        {
            return num1 * num2;
        }
        public double Divide(int num1, int num2)
        {
            try
            {
                //if (num2 != 0)
                    return num1 / num2;
                //else
                //    return 0;
            }
            catch (Exception ex)
            {
                CustomException cex = new CustomException();
                cex.Title = "Error Funtion:Divide()";
                cex.InnerException = "Inner exception message from serice";
                cex.ExceptionMessage = ex.Message;
                cex.StackTrace = ex.StackTrace;
                throw new FaultException<CustomException>(cex);
            }
        }
    }
}
