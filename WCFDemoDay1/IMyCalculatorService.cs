﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace WCFDemoDay1
{
    [ServiceContract]
    public interface IMyCalculatorService
    {
        [OperationContract]
        int Add(int Num1, int Num2);
        [OperationContract]
        int Subtract(int Num1, int Num2);
        [OperationContract]
        int Multiply(int num1, int num2);
        [OperationContract]
        [FaultContract(typeof(CustomException))]
        double Divide(int num1, int num2);
    }
    [DataContract]
    public class CustomException
    {
        [DataMember]
        public String Title { get; set; }
        [DataMember]
        public String ExceptionMessage { get; set; }
        [DataMember]
        public String InnerException { get; set; }
        [DataMember]
        public String StackTrace { get; set; }
    }
}