﻿using System.ServiceModel;

namespace WCFDemoDay1
{
    [ServiceContract]
    public interface IMyService
    {
        [OperationContract]
        int MyMethod();
    }
}
